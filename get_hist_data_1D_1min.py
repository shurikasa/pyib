# -*- coding: utf-8 -*-
# Import libraries
from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
import pandas as pd
import threading
import time
import numpy as np
import os.path

class TradeApp(EWrapper, EClient): 
    def __init__(self): 
        EClient.__init__(self, self) 
        self.data = {}
        
    def historicalData(self, reqId, bar):
        if reqId not in self.data:
            self.data[reqId] = [{"Date":bar.date,"Close":bar.close,"High":bar.high,"Low":bar.low,"Open":bar.open,"Volume":bar.volume}]
        else:
            self.data[reqId].append({"Date":bar.date,"Close":bar.close,"High":bar.high,"Low":bar.low,"Open":bar.open,"Volume":bar.volume})

def usTechStk(symbol, sec_type="STK", currency="USD", exchange="ISLAND"):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sec_type
    contract.currency = currency
    contract.exchange = exchange
    return contract 

def histData(req_num,contract,duration,candle_size):
    """extracts historical data"""
    app.reqHistoricalData(reqId=req_num, 
                          contract=contract,
                          endDateTime='',
                          durationStr=duration,
                          barSizeSetting=candle_size,
                          whatToShow='ADJUSTED_LAST',
                          useRTH=1,
                          formatDate=1,
                          keepUpToDate=0,
                          chartOptions=[])	 # EClient function to request contract details

def websocket_con():
    app.run()
    event.wait()
    if event.is_set():
        app.disconnect()

event = threading.Event()
app = TradeApp()
app.connect(host='127.0.0.1', port=7497, clientId=23) #port 4002 for ib gateway paper trading/7497 for TWS paper trading
con_thread = threading.Thread(target = websocket_con, daemon = True)
con_thread.start()
time.sleep(1) # some latency added to ensure that the connection is established

# Reading tickers from file
tickers = []
with open('tickers.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        tickers.append(line[:-1])
        line = reader.readline()
# Remove empty lines, if any
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

#tickers = ["FB"]

# Processing each ticker to get historical data
for ticker in tickers:
    try:
        print("Getting historical data for {}".format(ticker))
        histData(tickers.index(ticker), usTechStk(ticker), '1 D', '1 min')
        time.sleep(2)

        # Dumping data in csv files
        df = pd.DataFrame(app.data[tickers.index(ticker)])
        df.set_index("Date", inplace = True)
        df.to_csv("./data/" + ticker + "_1D_1min.csv")
        print("\tDumped csv file")
    except Exception as e:
        print(e)
        print("Unable to extract data for {}".format(ticker))
print("Finished collecting data")

time.sleep(1)
event.set()
quit()