# -*- coding: utf-8 -*-
# Import libraries
import pandas as pd
import threading
import time
from copy import deepcopy
import numpy as np
from joblib import load
import os.path

def MACD(DF,a=12,b=26,c=9):
    """function to calculate MACD
       typical values a(fast moving average) = 12; 
                      b(slow moving average) =26; 
                      c(signal line ma window) =9"""
    df = DF.copy()
    df["MA_Fast"]=df["Close"].ewm(span=a,min_periods=a).mean()
    df["MA_Slow"]=df["Close"].ewm(span=b,min_periods=b).mean()
    df["MACD"]=df["MA_Fast"]-df["MA_Slow"]
    df["Signal"]=df["MACD"].ewm(span=c,min_periods=c).mean()
    df.dropna(inplace=True)
    return df

def stochOscltr(DF, a=20, b=3):
    """function to calculate Stochastics
       a = lookback period
       b = moving average window for %D"""
    df = DF.copy()
    df['C-L'] = df['Close'] - df['Low'].rolling(a).min()
    df['H-L'] = df['High'].rolling(a).max() - df['Low'].rolling(a).min()
    df['%K'] = df['C-L']/df['H-L']*100
    #df['%D'] = df['%K'].ewm(span=b,min_periods=b).mean()
    return df['%K'].rolling(b).mean()

def atr(DF,n):
    "function to calculate True Range and Average True Range"
    df = DF.copy()
    df['H-L']=abs(df['High']-df['Low'])
    df['H-PC']=abs(df['High']-df['Close'].shift(1))
    df['L-PC']=abs(df['Low']-df['Close'].shift(1))
    df['TR']=df[['H-L','H-PC','L-PC']].max(axis=1,skipna=False)
    #df['ATR'] = df['TR'].rolling(n).mean()
    df['ATR'] = df['TR'].ewm(com=n,min_periods=n).mean()
    return df['ATR']

def comp_ret(DF):
    "function to calculate the compound return"
    return (1 + DF["ret"]).cumprod().tolist()[-1] - 1

def CAGR(DF, num_data_points):
    "function to calculate the Cumulative Annual Growth Rate of a trading strategy"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    n = len(df)/num_data_points
    CAGR = (df["cum_return"].tolist()[-1])**(1/n) - 1
    return CAGR

def volatility(DF, num_data_points):
    "function to calculate annualized volatility of a trading strategy"
    df = DF.copy()
    vol = df["ret"].std() * np.sqrt(num_data_points)
    return vol

def sharpe(DF, rf, num_data_points):
    "function to calculate sharpe ratio ; rf is the risk free rate"
    df = DF.copy()
    sr = (CAGR(df, num_data_points) - rf)/volatility(df, num_data_points)
    return sr

def max_dd(DF):
    "function to calculate max drawdown"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    df["cum_roll_max"] = df["cum_return"].cummax()
    df["drawdown"] = df["cum_roll_max"] - df["cum_return"]
    df["drawdown_pct"] = df["drawdown"]/df["cum_roll_max"]
    max_dd = df["drawdown_pct"].max()
    return max_dd

def pred(DF, reg, n_start = 1170):
    "function to predict values"
    preds = {}
    preds["High"] = reg.predict(DF["High"][-n_start:].values.reshape(1, -1))
    preds["Low"] = reg.predict(DF["Low"][-n_start:].values.reshape(1, -1))
    preds["Close"] = reg.predict(DF["Close"][-n_start:].values.reshape(1, -1))
    return preds

start_time = time.time()
tickers = []
with open('tickers_1D_1min.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

#tickers = ["FB"]

# Loading data from csv files
historicalData = {}
for ticker in tickers:
    try:
        #print("Loading historical data for {}".format(ticker))
        historicalData[ticker] = pd.read_csv("./data/" + ticker + ".csv", index_col = "Date")
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
print("Loaded data")

# Number of days * number of minutes
num_dps = 390

#####################################   BACKTESTING   ##################################
ohlc_dict = deepcopy(historicalData)
tickers_signal = {}
tickers_ret = {}
trade_count = {}
for ticker in tickers:
    print("Calculating MACD & Stochastics for ", ticker)

    # Predicting values for the whole day. Seems like each predicted column should be based on its own regressor
    regressor = load('./models/reg_' + ticker + '_1D_1min.joblib')
    preds = pred(ohlc_dict[ticker], regressor)
    print("\t Calculated pred")
    preds_df = pd.DataFrame(columns=["Date", "Close", "High", "Low", "Open", "Volume"])
    preds_df = preds_df.assign(Close=preds["Close"][0], High=preds["High"][0], Low=preds["Low"][0])
    preds_df.set_index("Date", inplace = True)
    #print(preds_df)
    # Updating overall table with predicted values
    ohlc_dict[ticker] = ohlc_dict[ticker].append(preds_df)
    #print(ohlc_dict[ticker])

    ohlc_dict[ticker]["stoch"] = stochOscltr(ohlc_dict[ticker])
    ohlc_dict[ticker]["macd"] = MACD(ohlc_dict[ticker])["MACD"]
    ohlc_dict[ticker]["signal"] = MACD(ohlc_dict[ticker])["Signal"]
    ohlc_dict[ticker]["atr"] = atr(ohlc_dict[ticker], 60)
    ohlc_dict[ticker].dropna(inplace=True)
    trade_count[ticker] = 0
    tickers_signal[ticker] = ""
    tickers_ret[ticker] = [0]
    
    
#Identifying Signals and calculating daily return (Stop Loss factored in)
for ticker in tickers:
    print("Calculating daily returns for ", ticker)

    # Adjusting for last number of specified days (5 days for now)
    n_start = len(ohlc_dict[ticker]) - num_dps
    n_end = len(ohlc_dict[ticker])
    for i in range(n_start, n_end):
        if tickers_signal[ticker] == "":
            tickers_ret[ticker].append(0)
            if ohlc_dict[ticker]["macd"][i] > ohlc_dict[ticker]["signal"][i] and \
               ohlc_dict[ticker]["stoch"][i] > 30 and \
               ohlc_dict[ticker]["stoch"][i] > ohlc_dict[ticker]["stoch"][i-1]:
                   tickers_signal[ticker] = "Buy"
                   trade_count[ticker]+=1
                     
        elif tickers_signal[ticker] == "Buy":
            if ohlc_dict[ticker]["Low"][i]<ohlc_dict[ticker]["Close"][i-1] - ohlc_dict[ticker]["atr"][i-1]:
                tickers_signal[ticker] = ""
                trade_count[ticker]+=1
                tickers_ret[ticker].append(((ohlc_dict[ticker]["Close"][i-1] - ohlc_dict[ticker]["atr"][i-1])/ohlc_dict[ticker]["Close"][i-1])-1)
            else:
                tickers_ret[ticker].append((ohlc_dict[ticker]["Close"][i]/ohlc_dict[ticker]["Close"][i-1])-1)
                
    orig_size = len(ohlc_dict[ticker].index) - len(tickers_ret[ticker])
    #print("size should be ", len(ohlc_dict[ticker].index))
    #print("adding ", len(tickers_ret[ticker]))
    ohlc_dict[ticker]["ret"] = np.concatenate((np.zeros(orig_size), np.array(tickers_ret[ticker])))

# calculating overall strategy's KPIs
strategy_df = pd.DataFrame()
for ticker in tickers:
    strategy_df[ticker] = ohlc_dict[ticker]["ret"]
strategy_df["ret"] = strategy_df.mean(axis=1)

"""
# creating dataframe with daily returns
strategy_df.index = pd.to_datetime(strategy_df.index)
strategy_df.index = strategy_df.index.tz_localize('Asia/Kolkata').tz_convert('America/Indiana/Petersburg')    
daily_ret_df = strategy_df["ret"].resample("D").sum(min_count=1).dropna()

# plotting daily returns
daily_ret_df.plot(kind="bar") 
"""

#comp_ret(strategy_df)
#CAGR(strategy_df, num_data_points)
#sharpe(strategy_df, 0.025, num_data_points)
#max_dd(strategy_df)  


# vizualization of strategy return
#(1+strategy_df["ret"]).cumprod().plot()


#calculating individual stock's KPIs
comp = {}
cagr = {}
sharpe_ratios = {}
max_drawdown = {}
num_trades = {}
for ticker in tickers:
    print("calculating KPIs for ", ticker)
    num_data_points = len(ohlc_dict[ticker].index)
    comp[ticker] = comp_ret(ohlc_dict[ticker])
    cagr[ticker] =  CAGR(ohlc_dict[ticker], num_data_points)
    sharpe_ratios[ticker] =  sharpe(ohlc_dict[ticker],0.025, num_data_points)
    max_drawdown[ticker] =  max_dd(ohlc_dict[ticker])
    num_trades[ticker] = trade_count[ticker]
    
KPI_df = pd.DataFrame(columns=["Ticker", "Compound", "CAGR", "Sharpe", "Max_Drawdown", "Num_Trades"])
KPI_df = KPI_df.assign(Ticker=tickers, Compound=comp.values(), CAGR=cagr.values(), Sharpe=sharpe_ratios.values(), Max_Drawdown=max_drawdown.values(), Num_Trades=num_trades.values())
KPI_df = KPI_df.sort_values(by=["Compound"], ascending=False)
KPI_df.set_index("Ticker", inplace = True)
KPI_df["Compound"] *= 100
KPI_df = KPI_df.round(decimals=6)
#KPI_df.style.format({"Compund": "{:.2%}"})
print(KPI_df)
KPI_df.to_csv("./performance/pred_1D_1min.csv")

print("--- %s seconds ---" % (time.time() - start_time))