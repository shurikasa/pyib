# -*- coding: utf-8 -*-
# Import libraries
import pandas as pd
import time
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from joblib import dump
import os.path

pd.options.mode.chained_assignment = None

start_time = time.time()

tickers = []
with open('tickers_1D_1min.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

#tickers = ["FB","AMZN","INTC","MSFT","AAPL","GOOG","CSCO","CMCSA","ADBE","NVDA",
#           "NFLX","PYPL","AMGN","AVGO","TXN","CHTR","QCOM","GILD","FISV","BKNG",
#           "INTU","ADP","CME","TMUS","MU"]

#tickers = ["FB"]

# Loading data from csv files
historicalData = {}
for ticker in tickers:
    try:
        print("Loading historical data for {}".format(ticker))
        historicalData[ticker] = pd.read_csv("./data/" + ticker + ".csv")
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
print("Loaded data")
    
# Training model
for ticker in tickers:
    print(f"Training model for {ticker}")
    df = historicalData[ticker][["Close"]]
    for d in range(1, 1560):
        col = "%dd" % d
        df[col] = df['Close'].shift(-1 * d)
    df.head()
    df = df.dropna()
    #print(df)

    X = df.iloc[:, :1170]
    y = df.iloc[:, 1170:]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

    regressor = RandomForestRegressor(
        n_estimators=100,
        criterion='mse',
        max_depth=None,
        min_samples_split=2,
        min_samples_leaf=1,
        min_weight_fraction_leaf=0.0,
        max_features='auto',
        max_leaf_nodes=None,
        min_impurity_decrease=0.0,
        min_impurity_split=None,
        bootstrap=True,
        oob_score=True,
        n_jobs=16,
        random_state=None,
        verbose=False,
        warm_start=False)
    regressor.fit(X_train, y_train)

    score = regressor.score(X_test, y_test)
    time.sleep(2)
    print(f"\tScore is {score}")

    fi = regressor.feature_importances_

    y_predicted = regressor.predict(X_test)

    # Dumping regressors
    dump(regressor, './models/reg_' + ticker + '_1D_1min.joblib')
    print(f"\tDumped regressor for {ticker}")
    print("--- %s seconds ---" % (time.time() - start_time))

time.sleep(2)
print("Finished training models")