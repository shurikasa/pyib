# -*- coding: utf-8 -*-
# Import libraries
from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
import threading
import pandas as pd
import time
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from joblib import load
import os.path

pd.options.mode.chained_assignment = None

class TradeApp(EWrapper, EClient): 
    def __init__(self): 
        EClient.__init__(self, self) 
        self.data = {}
        
    def historicalData(self, reqId, bar):
        if reqId not in self.data:
            self.data[reqId] = [{"Date":bar.date,"Open":bar.open,"High":bar.high,"Low":bar.low,"Close":bar.close,"Volume":bar.volume}]
        else:
            self.data[reqId].append({"Date":bar.date,"Open":bar.open,"High":bar.high,"Low":bar.low,"Close":bar.close,"Volume":bar.volume})
        #print("reqID:{}, date:{}, open:{}, high:{}, low:{}, close:{}, volume:{}".format(reqId,bar.date,bar.open,bar.high,bar.low,bar.close,bar.volume))

def usTechStk(symbol,sec_type="STK",currency="USD",exchange="ISLAND"):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sec_type
    contract.currency = currency
    contract.exchange = exchange
    return contract 

def histData(req_num,contract,duration,candle_size):
    """extracts historical data"""
    app.reqHistoricalData(reqId=req_num, 
                          contract=contract,
                          endDateTime='',
                          durationStr=duration,
                          barSizeSetting=candle_size,
                          whatToShow='ADJUSTED_LAST',
                          useRTH=1,
                          formatDate=1,
                          keepUpToDate=0,
                          chartOptions=[])	 # EClient function to request contract details

def websocket_con():
    app.run()
    event.wait()
    if event.is_set():
        app.disconnect()

###################storing trade app object in dataframe#######################
def dataDataframe(symbols,TradeApp_obj):
    "returns extracted historical data in dataframe format"
    df_data = {}
    for symbol in symbols:
        df_data[symbol] = pd.DataFrame(TradeApp_obj.data[symbols.index(symbol)])
        df_data[symbol].set_index("Date", inplace=True)
    return df_data
###############################################################################

def MACD(DF,a=12,b=26,c=9):
    """function to calculate MACD
       typical values a(fast moving average) = 12; 
                      b(slow moving average) =26; 
                      c(signal line ma window) =9"""
    df = DF.copy()
    df["MA_Fast"]=df["Close"].ewm(span=a,min_periods=a).mean()
    df["MA_Slow"]=df["Close"].ewm(span=b,min_periods=b).mean()
    df["MACD"]=df["MA_Fast"]-df["MA_Slow"]
    df["Signal"]=df["MACD"].ewm(span=c,min_periods=c).mean()
    df.dropna(inplace=True)
    return df

def stochOscltr(DF,a=20,b=3):
    """function to calculate Stochastics
       a = lookback period
       b = moving average window for %D"""
    df = DF.copy()
    df['C-L'] = df['Close'] - df['Low'].rolling(a).min()
    df['H-L'] = df['High'].rolling(a).max() - df['Low'].rolling(a).min()
    df['%K'] = df['C-L']/df['H-L']*100
    #df['%D'] = df['%K'].ewm(span=b,min_periods=b).mean()
    return df['%K'].rolling(b).mean()

def atr(DF,n):
    "function to calculate True Range and Average True Range"
    df = DF.copy()
    df['H-L']=abs(df['High']-df['Low'])
    df['H-PC']=abs(df['High']-df['Close'].shift(1))
    df['L-PC']=abs(df['Low']-df['Close'].shift(1))
    df['TR']=df[['H-L','H-PC','L-PC']].max(axis=1,skipna=False)
    #df['ATR'] = df['TR'].rolling(n).mean()
    df['ATR'] = df['TR'].ewm(com=n,min_periods=n).mean()
    return df['ATR']

def CAGR(DF, num_data_points):
    "function to calculate the Cumulative Annual Growth Rate of a trading strategy"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    n = len(df)/num_data_points
    CAGR = (df["cum_return"].tolist()[-1])**(1/n) - 1
    return CAGR

def volatility(DF, num_data_points):
    "function to calculate annualized volatility of a trading strategy"
    df = DF.copy()
    vol = df["ret"].std() * np.sqrt(num_data_points)
    return vol

def sharpe(DF, rf, num_data_points):
    "function to calculate sharpe ratio ; rf is the risk free rate"
    df = DF.copy()
    sr = (CAGR(df, num_data_points) - rf)/volatility(df, num_data_points)
    return sr

def max_dd(DF):
    "function to calculate max drawdown"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    df["cum_roll_max"] = df["cum_return"].cummax()
    df["drawdown"] = df["cum_roll_max"] - df["cum_return"]
    df["drawdown_pct"] = df["drawdown"]/df["cum_roll_max"]
    max_dd = df["drawdown_pct"].max()
    return max_dd

def pred(DF, reg, n_start = 1170):
    "function to predict values"
    preds = {}
    preds["High"] = reg.predict(DF["High"][-n_start:].values.reshape(1, -1))
    preds["Low"] = reg.predict(DF["Low"][-n_start:].values.reshape(1, -1))
    preds["Close"] = reg.predict(DF["Close"][-n_start:].values.reshape(1, -1))
    return preds


event = threading.Event()
app = TradeApp()
app.connect(host='127.0.0.1', port=7497, clientId=23) #port 4002 for ib gateway paper trading/7497 for TWS paper trading
con_thread = threading.Thread(target=websocket_con, daemon=True)
con_thread.start()
time.sleep(1) # some latency added to ensure that the connection is established

start_time = time.time()

tickers = []
with open('tickers_1D_1min.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

tickers = ["FB"]

for ticker in tickers:
    try:
        print("Getting 1 day historical data for {}".format(ticker))
        #histData(tickers.index(ticker),usTechStk(ticker),'1 Y', '15 mins')
        histData(tickers.index(ticker),usTechStk(ticker),'1 D', '1 min')
        time.sleep(5)
    except Exception as e:
        print(e)
        print("unable to extract data for {}".format(ticker))

    #extract and store historical data in dataframe
    historicalData_1D = pd.DataFrame(app.data[tickers.index(ticker)])
    historicalData_1D.set_index("Date", inplace=True)

    # Loading data from csv files
    try:
        print("Loading 1 month historical data for {}".format(ticker))
        historicalData = pd.read_csv("./data/" + ticker + ".csv")
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
    print("Loaded data")

    # Predicting values for the whole day. Seems like each predicted column should be based on its own regressor
    print("Predicting values for ", ticker)
    regressor = load('./models/reg_' + ticker + '_1D_1min.joblib')
    preds = pred(historicalData, regressor)
    print("\t Calculated pred")
    preds_df = pd.DataFrame(columns=["Close", "High", "Low"])
    preds_df = preds_df.assign(Close=preds["Close"][0], High=preds["High"][0], Low=preds["Low"][0])
    #preds_df.set_index("Date", inplace = True)

    # Comparing historical and predicted data
    print(f"Comparing values for {ticker}")
    arr_real = historicalData_1D["Close"].to_numpy()
    arr_pred = preds_df["Close"].to_numpy()
    comp_df = abs(arr_real - arr_pred) / arr_real
    comp_val = np.array([np.min(comp_df), np.max(comp_df), np.mean(comp_df)])
    comp_val = np.round_(comp_val[:], 3)
    print(f"\tMin delta: {comp_val[0]}\tMax delta: {comp_val[1]}\tMean delta: {comp_val[2]}")

time.sleep(2)
event.set()
print("--- %s seconds ---" % (time.time() - start_time))