# -*- coding: utf-8 -*-
# Import libraries
from ibapi.client import EClient
from ibapi.wrapper import EWrapper
from ibapi.contract import Contract
from ibapi.order import Order
import pandas as pd
import numpy as np
import threading
import time
from datetime import datetime
import pytz
from joblib import load
import os.path
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor

class TradeApp(EWrapper, EClient): 
    def __init__(self): 
        EClient.__init__(self, self) 
        self.data = {}
        self.pos_df = pd.DataFrame(columns=['Account', 'Symbol', 'SecType',
                                    'Currency', 'Position', 'Cost'])
        self.order_df = pd.DataFrame(columns=['PermId', 'ClientId', 'OrderId',
                                          'Account', 'Symbol', 'SecType',
                                          'Exchange', 'Action', 'OrderType',
                                          'TotalQty', 'CashQty', 'LmtPrice',
                                          'AuxPrice', 'Status'])
        self.pred_err_max = 0.0
    
    def clear_pos(self):
        app.pos_df = pd.DataFrame(columns=['Account', 'Symbol', 'SecType',
                                    'Currency', 'Position', 'Cost'])

    def clear_ord(self):
        app.order_df = pd.DataFrame(columns=['PermId', 'ClientId', 'OrderId',
                                      'Account', 'Symbol', 'SecType',
                                      'Exchange', 'Action', 'OrderType',
                                      'TotalQty', 'CashQty', 'LmtPrice',
                                      'AuxPrice', 'Status'])
        
    def historicalData(self, reqId, bar):
        if reqId not in self.data:
            self.data[reqId] = pd.DataFrame({"Date":[bar.date],"Close":[bar.close],"High":[bar.high],"Low":[bar.low],"Open":[bar.open],"Volume":[bar.volume]})
            self.data[reqId].set_index("Date", inplace = True)
        elif datetime.strptime(bar.date, '%Y%m%d  %H:%M:%S') > datetime.strptime(self.data[reqId].index[-1], '%Y%m%d  %H:%M:%S'):
            new_df = pd.DataFrame({"Date":[bar.date],"Close":[bar.close],"High":[bar.high],"Low":[bar.low],"Open":[bar.open],"Volume":[bar.volume]})
            new_df.set_index("Date", inplace = True)
            self.data[reqId] = self.data[reqId].append(new_df)
            #print("Adding date ", bar.date)

    def nextValidId(self, orderId):
        super().nextValidId(orderId)
        self.nextValidOrderId = orderId
        #print("NextValidId:", orderId)
        
    def position(self, account, contract, position, avgCost):
        super().position(account, contract, position, avgCost)
        dictionary = {"Account":account, "Symbol": contract.symbol, "SecType": contract.secType,
                      "Currency": contract.currency, "Position": position, "Cost": avgCost}
        self.pos_df = self.pos_df.append(dictionary, ignore_index=True)
        
    def positionEnd(self):
        print("Latest position data extracted")
        
    def openOrder(self, orderId, contract, order, orderState):
        super().openOrder(orderId, contract, order, orderState)
        dictionary = {"PermId":order.permId, "ClientId": order.clientId, "OrderId": orderId, 
                      "Account": order.account, "Symbol": contract.symbol, "SecType": contract.secType,
                      "Exchange": contract.exchange, "Action": order.action, "OrderType": order.orderType,
                      "TotalQty": order.totalQuantity, "CashQty": order.cashQty, 
                      "LmtPrice": order.lmtPrice, "AuxPrice": order.auxPrice, "Status": orderState.status}
        self.order_df = self.order_df.append(dictionary, ignore_index=True)
        


def usTechStk(symbol,sec_type="STK",currency="USD",exchange="ISLAND"):
    contract = Contract()
    contract.symbol = symbol
    contract.secType = sec_type
    contract.currency = currency
    contract.exchange = exchange
    return contract 

def histData(req_num, contract, duration, candle_size):
    """extracts historical data"""
    app.reqHistoricalData(reqId=req_num, 
                          contract=contract,
                          endDateTime='',
                          durationStr=duration,
                          barSizeSetting=candle_size,
                          whatToShow='ADJUSTED_LAST',
                          useRTH=1,
                          formatDate=1,
                          keepUpToDate=0,
                          chartOptions=[])	 # EClient function to request contract details

# Training model
def train(historicalData, tickers):
    regressors = {}
    for ticker in tickers:
        print(f"Training model for {ticker}")
        df = historicalData[tickers.index(ticker)][["Close"]].copy()
        for d in range(1, 41):
            col = "%dd" % d
            df[col] = df['Close'].shift(-1 * d)
        df.head()
        df = df.dropna()
        #print(df)

        X = df.iloc[:, :41 - num_preds]
        y = df.iloc[:, 41 - num_preds:]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)

        reg = RandomForestRegressor(
            n_estimators=100,
            criterion='mse',
            max_depth=None,
            min_samples_split=2,
            min_samples_leaf=1,
            min_weight_fraction_leaf=0.0,
            max_features='auto',
            max_leaf_nodes=None,
            min_impurity_decrease=0.0,
            min_impurity_split=None,
            bootstrap=True,
            oob_score=True,
            n_jobs=16,
            random_state=None,
            verbose=False,
            warm_start=False)
        if num_preds == 1:
            reg.fit(X_train, y_train.values.ravel())
        else:
            reg.fit(X_train, y_train)

        score = reg.score(X_test, y_test)
        print(f"\tScore is {score}")

        #fi = reg.feature_importances_
        #y_predicted = reg.predict(X_test)

        regressors[tickers.index(ticker)] = reg
    
    return regressors

def pred_uptrend(preds, real_close):
    if preds is None:
        return True
    chk_list = [real_close] + preds
    if (chk_list == sorted(chk_list, reverse=False)).all():
        return True
    else:
        return False

def websocket_con():
    app.run()

###################storing trade app object in dataframe#######################

def MACD(DF, a=12, b=26, c=9):
    """function to calculate MACD
       typical values a(fast moving average) = 12; 
                      b(slow moving average) =26; 
                      c(signal line ma window) =9"""
    df = DF.copy()
    df["MA_Fast"]=df["Close"].ewm(span=a,min_periods=a).mean()
    df["MA_Slow"]=df["Close"].ewm(span=b,min_periods=b).mean()
    df["MACD"]=df["MA_Fast"]-df["MA_Slow"]
    df["Signal"]=df["MACD"].ewm(span=c,min_periods=c).mean()
    return df

def stochOscltr(DF, a=20, b=3):
    """function to calculate Stochastics
       a = lookback period
       b = moving average window for %D"""
    df = DF.copy()
    df['C-L'] = df['Close'] - df['Low'].rolling(a).min()
    df['H-L'] = df['High'].rolling(a).max() - df['Low'].rolling(a).min()
    df['%K'] = df['C-L']/df['H-L']*100
    #df['%D'] = df['%K'].ewm(span=b,min_periods=b).mean()
    return df['%K'].rolling(b).mean()

def atr(DF, n):
    "function to calculate True Range and Average True Range"
    df = DF.copy()
    df['H-L']=abs(df['High']-df['Low'])
    df['H-PC']=abs(df['High']-df['Close'].shift(1))
    df['L-PC']=abs(df['Low']-df['Close'].shift(1))
    df['TR']=df[['H-L','H-PC','L-PC']].max(axis=1, skipna=False)
    #df['ATR'] = df['TR'].rolling(n).mean()
    df['ATR'] = df['TR'].ewm(com=n, min_periods=n).mean()
    return df['ATR']

def marketOrder(direction, quantity):
    order = Order()
    order.action = direction
    order.orderType = "MKT"
    order.totalQuantity = quantity
    return order

def limitOrder(direction, quantity, lmt_price):
    order = Order()
    order.action = direction
    order.orderType = "LMT"
    order.totalQuantity = quantity
    order.lmtPrice = lmt_price
    return order

def stopOrder(direction, quantity, st_price):
    order = Order()
    order.action = direction
    order.orderType = "STP"
    order.totalQuantity = quantity
    order.auxPrice = st_price
    return order

def no_position(app, ticker):
    return len(app.pos_df.columns) == 0 or \
            ticker not in app.pos_df["Symbol"].tolist() or \
            app.pos_df[app.pos_df["Symbol"]==ticker]["Position"].sort_values(ascending=True).values[-1] == 0

def position(app, ticker):
    return app.pos_df[app.pos_df["Symbol"] == ticker]["Position"].sort_values(ascending = True).values[-1] > 0

def cancelSellOrders(app, ticker):
    order_ids = app.order_df[app.order_df["Symbol"] == ticker]["OrderId"].sort_values(ascending=True).values
    for order_id in order_ids:
        app.cancelOrder(order_id)
        print(f"\t\t Cancelled sell order #{order_id}")

def cancelRedundantSellOrders(app, ticker, order_id):
    app.clear_ord()
    app.reqOpenOrders()
    time.sleep(1)
    order_ids = app.order_df[app.order_df["Symbol"] == ticker]["OrderId"].sort_values(ascending=True).values
    for oid in order_ids:
        if oid != order_id:
            app.cancelOrder(oid)
            print(f"\t\t Cancelled redundant sell order #{oid}")

def main(app, tickers, regressors, capital, num_preds):
    app.clear_pos()
    app.clear_ord()

    app.reqPositions()
    time.sleep(1)
    app.pos_df.drop_duplicates(inplace = True, ignore_index = True) # position callback tends to give duplicate values
    print("Got positions")

    app.reqOpenOrders()
    time.sleep(1)
    print("Got open orders")

    print("Positions: ", app.pos_df)
    print("Orders: ", app.order_df)

    for ticker in tickers:
        print("Starting analysis for ", ticker)
        df = app.data[tickers.index(ticker)]
        num_dps = len(df.index)
        pred_prev = regressors[tickers.index(ticker)].predict(df["Close"][-40:].values.reshape(1, -1))
        histData(tickers.index(ticker), usTechStk(ticker), '60 S', '1 min')
        time.sleep(1)
        df = app.data[tickers.index(ticker)]

        if len(df.index) - num_dps > 1:
            print("\tError!!! Data was not increased correctly")
            print(f"\tAfter increase: {len(df.index)}, before increase: {num_dps}")
        
        # Prediction error
        last_close = df["Close"][-1]
        err_close = round(abs(pred_prev[0] - last_close) / last_close, 3)
        pred_close = round(pred_prev[0], 2)
        print(f"\tPredicted close: {pred_close}, real close: {last_close}, delta: {err_close}")
        if (err_close > app.pred_err_max):
            app.pred_err_max = err_close

        df["stoch"] = stochOscltr(df)
        df["macd"] = MACD(df)["MACD"]
        df["signal"] = MACD(df)["Signal"]
        df["atr"] = atr(df, 60)
        df.dropna(inplace=True)

        quantity = int(capital / df["Close"][-1])
        if quantity == 0:
            continue

        if no_position(app, ticker):
            preds = regressors[tickers.index(ticker)].predict(df["Close"][-41+num_preds:].values.reshape(1, -1))
            prev_macd = round(df["macd"][-1], 4)
            prev_stoch = round(df["stoch"][-1], 2)
            prev_signal = round(df["signal"][-1], 4)
            prev_atr = round(df["atr"][-1], 4)
            pred_close = round(preds[0], 2)
            print(f"\tNext predicted: {pred_close}, MACD: {prev_macd}, Signal: {prev_signal}, Stoch: {prev_stoch}, ATR: {prev_atr}")
            if df["macd"][-1]> df["signal"][-1] and \
               df["stoch"][-1]> 30 and \
               df["stoch"][-1] > df["stoch"][-2] and \
               pred_uptrend(preds, df["Close"][-1]):
                    app.reqIds(-1)
                    time.sleep(1)
                    order_id = app.nextValidOrderId
                    buy_lmt = round(preds[0], 2)
                    app.placeOrder(order_id, usTechStk(ticker), limitOrder("BUY", quantity, buy_lmt))

                    # Setting buy order. If it's not filled for 5 seconds, we cancel it. Otherwise we place a sell order next to it
                    for i in range(5):
                        app.clear_pos()
                        app.reqPositions()
                        time.sleep(1)
                        app.pos_df.drop_duplicates(inplace = True, ignore_index = True)
                        print(app.pos_df)
                        if no_position(app, ticker):
                            if i < 4:
                                continue
                            app.cancelOrder(order_id)
                        else:
                            sell_lmt = round(app.pos_df[app.pos_df["Symbol"]==ticker]["Cost"].sort_values(ascending=True).values[-1] - df["atr"][-1], 2)
                            print(f"\t\tBuy order #{order_id}, stop loss sell order #{order_id+1}")
                            print(f"\t\tBuy limit: {buy_lmt}, sell limit: {sell_lmt}")
                            app.placeOrder(order_id+1, usTechStk(ticker), stopOrder("SELL", quantity, sell_lmt))
                            break
                   
        elif position(app, ticker):
            # Don't move stop loss if the price got lower
            if df["Close"][-1] <= app.pos_df[app.pos_df["Symbol"]==ticker]["Position"].values[0]:
                continue

            # Also don't change it if the new loss limit is smaller than the previous one
            sell_lmt = round(df["Close"][-1] - df["atr"][-1], 2)
            if sell_lmt <= app.order_df[app.order_df["Symbol"]==ticker]["AuxPrice"].values[0]:
                continue

            # Cancel orders. Could be more than one if by chance there are redundant ones that could cause shorting
            old_quantity = app.pos_df[app.pos_df["Symbol"] == ticker]["Position"].sort_values(ascending=True).values[-1]
            cancelSellOrders(app, ticker)
            app.reqIds(-1)
            time.sleep(1)
            order_id = app.nextValidOrderId
            app.placeOrder(order_id,usTechStk(ticker),stopOrder("SELL", old_quantity, round(df["Close"][-1]-df["atr"][-1],2)))
            print(f"\t\tNew sell limit: {sell_lmt}")

            # Look once again for redundant sell orders and cancel them
            cancelRedundantSellOrders(app, ticker, order_id)


# Initialize app data, extract historical data and run trading
app = TradeApp()
app.connect(host='127.0.0.1', port=7497, clientId=23) #port 4002 for ib gateway paper trading/7497 for TWS paper trading
con_thread = threading.Thread(target=websocket_con, daemon=True)
con_thread.start()
print("Started app")

#tickers = ["FB","AMZN","INTC","MSFT","AAPL","GOOG","CSCO","CMCSA","ADBE","NVDA",
#           "NFLX","PYPL","AMGN","AVGO","TXN","CHTR","QCOM","GILD","FISV","BKNG",
#           "INTU","ADP","CME","TMUS","MU"]

# Reading tickers from file
tickers = []
with open('tickers_live.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

tickers = ["FB", "TSLA", "AMZN"]

capital = 1e+4

# Load historical data before today
app.data = {}
for ticker in tickers:
    try:
        print("Loading historical data for {}".format(ticker))
        app.data[tickers.index(ticker)] = pd.read_csv("./data/" + ticker + ".csv", index_col = "Date")
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
print("Loaded historical data")
num_dps = len(app.data[0].index)
print("Number of history data points: ", num_dps)

regressors = {}
num_preds = 2

# Assume we have historical date until previous day of trading
# Add historical data of today if we started later than market open
tz_NY = pytz.timezone('America/New_York')
datetime_now = datetime.now(tz_NY)
datetime_market_open = datetime_now.replace(hour=9, minute=30, second=0)
datetime_trading_open = datetime_now.replace(hour=10, minute=30, second=0)
print(f"trading open {datetime_trading_open}, time now {datetime_now}")

# Wait if market is not open
if (datetime_now < datetime_trading_open):
    seconds_to_wait = (datetime_trading_open - datetime_now).total_seconds()
    print(f"Sleeping {seconds_to_wait} seconds until the market is open")
    time.sleep(seconds_to_wait)
else:
    # Update historical data and train models on it
    for ticker in tickers:
        histData(tickers.index(ticker), usTechStk(ticker), '1 D', '1 min')
        time.sleep(2)
    print("Brought data up-to-date")
    n_hist_dp = len(app.data[0].index)
    print("Updated number of history data points: ", n_hist_dp)

    start_time = time.time()
    regressors = train(app.data, tickers, num_preds)
    training_time = int(time.time() - start_time)
    print("Trained data in %s seconds" % (training_time))

    # Training data might take some time, updating data before training and now (assuming no more than 5 mins)
    histData(tickers.index(ticker), usTechStk(ticker), str(training_time + 30) + ' S', '1 min')

# Start trading
print("Started trading")
datetime_trading_close = datetime_now.replace(hour=15, minute=30, second=0)
starttime = time.time()
while datetime_now <= datetime_trading_close:
    print("\n\nTime: ", datetime_now)

    main(app, tickers, regressors, capital, num_preds)
    
    # Sleep until end of each minute
    time.sleep(60 - ((time.time() - starttime) % 60.0))
    datetime_now = datetime.now(tz_NY)


# Cancelling open orders
app.reqGlobalCancel()

# Closing off open positions (longs for now)
order_id = app.nextValidOrderId
app.reqPositions()
time.sleep(2)
pos_df = app.pos_df
pos_df.drop_duplicates(inplace=True, ignore_index=True)

for ticker in pos_df["Symbol"]:
    quantity = pos_df[pos_df["Symbol"]==ticker]["Position"].values[0]
    app.placeOrder(order_id, usTechStk(ticker), marketOrder("SELL",quantity)) # EClient function to request contract details
    order_id += 1

time.sleep(5)
print("Finished trading")
