# -*- coding: utf-8 -*-
# Import libraries
import threading
import pandas as pd
import time
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from joblib import load, dump
import os.path
from copy import deepcopy

pd.options.mode.chained_assignment = None

def MACD(DF,a=12,b=26,c=9):
    """function to calculate MACD
       typical values a(fast moving average) = 12; 
                      b(slow moving average) =26; 
                      c(signal line ma window) =9"""
    df = DF.copy()
    df["MA_Fast"]=df["Close"].ewm(span=a,min_periods=a).mean()
    df["MA_Slow"]=df["Close"].ewm(span=b,min_periods=b).mean()
    df["MACD"]=df["MA_Fast"]-df["MA_Slow"]
    df["Signal"]=df["MACD"].ewm(span=c,min_periods=c).mean()
    df.dropna(inplace=True)
    return df

def stochOscltr(DF,a=20,b=3):
    """function to calculate Stochastics
       a = lookback period
       b = moving average window for %D"""
    df = DF.copy()
    df['C-L'] = df['Close'] - df['Low'].rolling(a).min()
    df['H-L'] = df['High'].rolling(a).max() - df['Low'].rolling(a).min()
    df['%K'] = df['C-L']/df['H-L']*100
    #df['%D'] = df['%K'].ewm(span=b,min_periods=b).mean()
    return df['%K'].rolling(b).mean()

def atr(DF,n):
    "function to calculate True Range and Average True Range"
    df = DF.copy()
    df['H-L']=abs(df['High']-df['Low'])
    df['H-PC']=abs(df['High']-df['Close'].shift(1))
    df['L-PC']=abs(df['Low']-df['Close'].shift(1))
    df['TR']=df[['H-L','H-PC','L-PC']].max(axis=1,skipna=False)
    #df['ATR'] = df['TR'].rolling(n).mean()
    df['ATR'] = df['TR'].ewm(com=n,min_periods=n).mean()
    return df['ATR']

def comp_ret(DF):
    "function to calculate the compound return"
    return (1 + DF["ret"]).cumprod().tolist()[-1] - 1

def CAGR(DF, num_data_points):
    "function to calculate the Cumulative Annual Growth Rate of a trading strategy"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    n = len(df)/num_data_points
    CAGR = (df["cum_return"].tolist()[-1])**(1/n) - 1
    return CAGR

def volatility(DF, num_data_points):
    "function to calculate annualized volatility of a trading strategy"
    df = DF.copy()
    vol = df["ret"].std() * np.sqrt(num_data_points)
    return vol

def sharpe(DF, rf, num_data_points):
    "function to calculate sharpe ratio ; rf is the risk free rate"
    df = DF.copy()
    sr = (CAGR(df, num_data_points) - rf)/volatility(df, num_data_points)
    return sr

def max_dd(DF):
    "function to calculate max drawdown"
    df = DF.copy()
    df["cum_return"] = (1 + df["ret"]).cumprod()
    df["cum_roll_max"] = df["cum_return"].cummax()
    df["drawdown"] = df["cum_roll_max"] - df["cum_return"]
    df["drawdown_pct"] = df["drawdown"]/df["cum_roll_max"]
    max_dd = df["drawdown_pct"].max()
    return max_dd

def pred(DF, reg, num_preds, n_start = 21):
    "function to predict values"
    preds = []
    for i in range(300, 0, -1):
        #print("Predicting series ", i)
        preds_vals = reg.predict(DF["High"][-n_start+num_preds-i:-i].values.reshape(1, -1))
        preds.append(preds_vals[0])
        #preds["Low"] = reg.predict(DF["Low"][-n_start-i:-i].values.reshape(1, -1))
        #preds["Close"] = reg.predict(DF["Close"][-n_start-i:-i].values.reshape(1, -1))
    return preds

def train(historicalData, ticker, num_preds):
    # Training model
    print(f"Training model for {ticker}")
    df = historicalData[["Close"]]
    for d in range(1, 21):
        col = "%dd" % d
        df[col] = df['Close'].shift(-1 * d)
    df.head()
    df = df.dropna()
    #print(df)

    X = df.iloc[:, :21-num_preds]
    y = df.iloc[:, 21-num_preds:]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)

    print("X train shape: ", X_train.shape)
    print("X test shape: ", X_test.shape)
    print("y train shape: ", y_train.shape)
    print("y test shape: ", y_test.shape)

    regressor = RandomForestRegressor(
        n_estimators=100,
        criterion='mse',
        max_depth=None,
        min_samples_split=2,
        min_samples_leaf=1,
        min_weight_fraction_leaf=0.0,
        max_features='auto',
        max_leaf_nodes=None,
        min_impurity_decrease=0.0,
        min_impurity_split=None,
        bootstrap=True,
        oob_score=True,
        n_jobs=16,
        random_state=None,
        verbose=False,
        warm_start=False)
    regressor.fit(X_train, y_train)

    score = regressor.score(X_test, y_test)
    time.sleep(1)
    print(f"\tScore is {score}")

    #fi = regressor.feature_importances_

    #y_predicted = regressor.predict(X_test)

    # Dumping regressors
    dump(regressor, './models/reg_' + ticker + '_max_min_1min.joblib')
    print(f"\tDumped regressor for {ticker}")
    print("--- %s seconds ---" % (time.time() - start_time))

def pred_uptrend(pred, real_close):
    if pred is None:
        return True
    chk_list = [real_close] + pred
    if (chk_list == sorted(chk_list, reverse=False)).all():
        return True
    else:
        return False


# This function finds the buy sell
# schedule for maximum profit
def stockBuySell(price_data):
	n = len(price_data)
	i = 0
	while (i < (n - 1)):
		
		# Find Local Minima
		# Note that the limit is (n-2) as we are
		# comparing present element to the next element
		while ((i < (n - 1)) and
				(price_data[i + 1] <= price_data[i])):
			i += 1
		
		# If we reached the end, break
		# as no further solution possible
		if (i == n - 1):
			break
		
		# Store the index of minima
		buy = i
		i += 1
		
		# Find Local Maxima
		# Note that the limit is (n-1) as we are
		# comparing to previous element
		while ((i < n) and (price_data[i] >= price_data[i - 1])):
			i += 1
			
		# Store the index of maxima
		sell = i - 1
		
		print("Buy: ", buy,"\tSell: ", sell, "\tDiff: ", price_data[sell]-price_data[buy])

start_time = time.time()

tickers = []
with open('tickers_1D_1min.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

tickers = ["AMZN"]

for ticker in tickers:
    # Loading data from csv files
    try:
        print("Loading 1 month historical data for {}".format(ticker))
        historicalData = pd.read_csv("./data/" + ticker + ".csv")
        historicalData = historicalData[:-30]
        #print(historicalData)
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
    print("Loaded data")

    print("Training model")
    num_preds = 2
    train(historicalData, ticker, num_preds)

    ohlc_dict = deepcopy(historicalData)
    print("Calculating MACD & Stochastics for ", ticker)
    ohlc_dict["stoch"] = stochOscltr(ohlc_dict)
    ohlc_dict["macd"] = MACD(ohlc_dict)["MACD"]
    ohlc_dict["signal"] = MACD(ohlc_dict)["Signal"]
    ohlc_dict["atr"] = atr(ohlc_dict, 60)
    
    # Predicting values for the whole day. Seems like each predicted column should be based on its own regressor
    print("Predicting values for ", ticker)
    regressor = load('./models/reg_' + ticker + '_max_min_1min.joblib')
    preds = pred(historicalData, regressor, num_preds)
    print("\t Calculated pred")
    ohlc_dict.dropna(inplace=True)
    trade_count = 0
    tickers_signal = ""
    tickers_ret = [0]

    print("Calculating daily returns for ", ticker)
    ohlc_dict = ohlc_dict[-301:-1]
    ohlc_dict.reset_index(inplace=True)

    # Adjusting for last number of specified days (5 days for now)
    buy_sell = []
    for i in range(len(ohlc_dict.index) - num_preds):
        if tickers_signal == "":
            tickers_ret.append(0)
            if ohlc_dict["macd"][i] > ohlc_dict["signal"][i] and \
               ohlc_dict["stoch"][i] > 30 and \
               ohlc_dict["stoch"][i] > ohlc_dict["stoch"][i-1] and \
               pred_uptrend(preds[i+1], ohlc_dict["Close"][i]):
#               pred_uptrend(None, ohlc_dict["Close"][i]):
#               pred_uptrend(preds[i+1], ohlc_dict["Close"][i]):
                   tickers_signal = "Buy"
                   trade_count+=1
                   buy_sell.append([i, 0])
                     
        elif tickers_signal == "Buy":
            if ohlc_dict["Low"][i]<ohlc_dict["Close"][i-1] - ohlc_dict["atr"][i-1]:
                tickers_signal = ""
                trade_count+=1
                buy_sell[-1][1] = i
                tickers_ret.append(((ohlc_dict["Close"][i-1] - ohlc_dict["atr"][i-1])/ohlc_dict["Close"][i-1])-1)
            else:
                tickers_ret.append((ohlc_dict["Close"][i]/ohlc_dict["Close"][i-1])-1)
                
    if buy_sell[-1][1] == 0:
        last_ind = len(ohlc_dict.index) - num_preds - 1
        buy_sell[-1][1] = last_ind
        trade_count+=1
        tickers_ret.append(((ohlc_dict["Close"][last_ind] - ohlc_dict["atr"][last_ind])/ohlc_dict["Close"][last_ind])-1)
    orig_size = len(ohlc_dict.index) - len(tickers_ret)
    ohlc_dict["ret"] = np.concatenate((np.zeros(orig_size), np.array(tickers_ret)))

    num_data_points = len(ohlc_dict.index)
    comp = comp_ret(ohlc_dict) * 100
    cagr = CAGR(ohlc_dict, num_data_points)
    sharpe_ratios = sharpe(ohlc_dict, 0.025, num_data_points)
    max_drawdown = max_dd(ohlc_dict)
    num_trades = trade_count

    print(f"Compound return: {comp}, Number of trades: {num_trades}")
    print(f"Buy / sell: {buy_sell}")
    buy_sell_prices = []
    for bs in buy_sell:
        buy_sell_prices.append([ohlc_dict["Close"][bs[0]], ohlc_dict["Close"][bs[1]]])
    print(f"Buy / sell prices: {buy_sell_prices}")

    historicalData = historicalData[-301:-1]
    historicalData.reset_index(inplace=True)
    #print(preds)
    print("\t Calculated pred")
    preds_df = pd.DataFrame(columns = ["Close"])
    preds_first = []
    preds_last = []
    for elem in preds:
        preds_first.append(elem[0])
        preds_last.append(elem[-1])
    preds_df = preds_df.assign(Close = preds_first)
    #preds_df = preds_df[:-1]
    #preds_df.set_index("Date", inplace = True)

    # Comparing historical and predicted data for 300 min session
    print(f"Comparing values for {ticker} first")
    arr_real = historicalData["Close"].to_numpy()
    arr_pred = preds_df["Close"].to_numpy()
    comp_df = abs(arr_real - arr_pred) / arr_real
    comp_val = np.array([np.min(comp_df), np.max(comp_df), np.mean(comp_df)])
    comp_val = np.round_(comp_val[:], 4)
    print(f"\tMin delta: {comp_val[0]}\tMax delta: {comp_val[1]}\tMean delta: {comp_val[2]}")

    #stockBuySell(arr_pred)
    #idx = preds_df.index.values
    #idx += 60
    #preds_df = preds_df.iloc[::-1]
    #preds_df.index = idx
    #val = preds_df["Close"].values[0]
    #print(val)
    #preds_df["Close"] = 2*val - preds_df["Close"]
    #ax = arr_pred.plot()
    #arr_real.plot(ax=ax).get_figure().savefig(ticker + '_chart.png')
    ax = preds_df["Close"].plot()
    historicalData["Close"].plot(ax=ax).get_figure().savefig(ticker + '_first.png')
    diff_df = (historicalData["Close"] - preds_df["Close"]) / historicalData["Close"]
    diff_df.plot().clear()
    diff_df.plot().get_figure().savefig(ticker + '_first_diff.png')
    #diff_df = pd.DataFrame(columns = ["Diff"])
    #diff_df.assign(Diff = )

    preds_df = preds_df[:-1]
    preds_df = preds_df.assign(Close = preds_last[:-1])
    historicalData = historicalData[-299:]
    historicalData.reset_index(inplace=True)
    preds_df["Close"].plot().clear()
    ax = preds_df["Close"].plot()
    historicalData["Close"].plot(ax=ax).get_figure().savefig(ticker + '_last.png')
    diff_df = (historicalData["Close"] - preds_df["Close"]) / historicalData["Close"]
    diff_df.plot().clear()
    diff_df.plot().get_figure().savefig(ticker + '_last_diff.png')

    print(f"Comparing values for {ticker} last")
    arr_real = historicalData["Close"].to_numpy()
    arr_pred = preds_df["Close"].to_numpy()
    comp_df = abs(arr_real - arr_pred) / arr_real
    comp_val = np.array([np.min(comp_df), np.max(comp_df), np.mean(comp_df)])
    comp_val = np.round_(comp_val[:], 4)
    print(f"\tMin delta: {comp_val[0]}\tMax delta: {comp_val[1]}\tMean delta: {comp_val[2]}")

print("--- %s seconds ---" % (time.time() - start_time))