# -*- coding: utf-8 -*-
# Import libraries
import pandas as pd
import time
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from joblib import dump
import os.path

pd.options.mode.chained_assignment = None

tickers = []
with open('tickers.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        ticker = line[:-1]
        if os.path.exists("./data/" + ticker + ".csv"):
            tickers.append(ticker)
        line = reader.readline()
tickers = list(filter(None, tickers))
print("Tickers: {}".format(tickers))

#tickers = ["FB","AMZN","INTC","MSFT","AAPL","GOOG","CSCO","CMCSA","ADBE","NVDA",
#           "NFLX","PYPL","AMGN","AVGO","TXN","CHTR","QCOM","GILD","FISV","BKNG",
#           "INTU","ADP","CME","TMUS","MU"]

tickers = ["FB", "AMZN", "TSLA"]

# Loading data from csv files
historicalData = {}
for ticker in tickers:
    try:
        print("Loading historical data for {}".format(ticker))
        historicalData[ticker] = pd.read_csv("./data/" + ticker + ".csv")
    except Exception as e:
        print(e)
        print("Unable to load data for {}".format(ticker))
print("Loaded data")
    
# Training model
def train(historicalData, tickers, num_preds):
    regressors = {}
    for ticker in tickers:
        print(f"Training model for {ticker}")
        df = historicalData[ticker][["Close"]].copy()
        for d in range(1, 41):
            col = "%dd" % d
            df[col] = df['Close'].shift(-1 * d)
        df.head()
        df = df.dropna()
        #print(df)

        X = df.iloc[:, :41 - num_preds]
        y = df.iloc[:, 41 - num_preds:]

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4)

        reg = RandomForestRegressor(
            n_estimators=100,
            criterion='mse',
            max_depth=None,
            min_samples_split=2,
            min_samples_leaf=1,
            min_weight_fraction_leaf=0.0,
            max_features='auto',
            max_leaf_nodes=None,
            min_impurity_decrease=0.0,
            min_impurity_split=None,
            bootstrap=True,
            oob_score=True,
            n_jobs=16,
            random_state=None,
            verbose=False,
            warm_start=False)

        if num_preds == 1:
            reg.fit(X_train, y_train.values.ravel())
        else:
            reg.fit(X_train, y_train)

        score = reg.score(X_test, y_test)
        print(f"\tScore is {score}")

        #fi = reg.feature_importances_
        #y_predicted = reg.predict(X_test)

        regressors[ticker] = reg
    
    return regressors

num_preds = 2
regressors = train(historicalData, tickers, num_preds)
# Dumping regressors
for ticker in tickers:
    dump(regressors[ticker], './models/reg_' + ticker + '.joblib')
    print(f"\tDumped regressor for {ticker}")

time.sleep(2)
print("Finished training models")