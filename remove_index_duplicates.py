# -*- coding: utf-8 -*-
# Import libraries

# Reading tickers from file
tickers = []
with open('tickers_all.txt') as reader:
    line = reader.readline()
    while line != '':  # The EOF char is an empty string
        tickers.append(line)
        line = reader.readline()

# Remove duplicates, if any
tickers = list(set(tickers))
print("Tickers: {}".format(tickers))
print("Number of tickers: ", len(tickers))

with open('tickers_all.txt', 'w') as writer:
    writer.writelines(list(filter(None, tickers)))
    writer.write("\n")

